# Dissertation

  

## Run project locally.

1. On a Linux machine create a folder in terminal `> mkdir folder_name` where you wish to download the Flask application and move into the folder `> cd folder_name`

2. Clone the git repository using:

	- `git clone https://gitlab.com/sc18sph/dissertation.git`

3. Move into the cloned directory

	- `cd dissertation`

4. If you do not have the virtualenv library download then download using:

	- `pip install virutalenv`

5. Create a virtual environment inside Dissertation/

	- `virtualenv venv`

6. Activate the virtualenv

    - `source venv/bin/activate`

7. Install the requirements:

	- `pip install -r requirements.txt`

8. Initialise the database:

	- `python database_init.py`

9. Run the flask app:

	- `flask run`

*Note: If you get an error message “Address already in use” then change the port that you run the project on with:*
	- `export FLASK_RUN_PORT=8000`

10. Once done go to a browser of your choice and go to http://127.0.0.1:5000/ where you will be greeted with a register and login page.

*Note: If you had to change the port in step 9 then replace 5000 with your chosen port in the ur

If you ran python database_init.py 9 users will have been created for you with usernames:
["admin", "sam", "chloe", "finn", "ella", "jude", "kai", "wren", "leo"]
The passwords for each account is the same as the username, sam is the only current teacher, the rest are students 
