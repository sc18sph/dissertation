"""
Python script that will add users, graphs and classes to the database to save time
"""


print(">>>Please allow ~15 seconds for database to be created and populated")
from project import *

app = create_app()
app.app_context().push()
print(">>>Creating databases")
db.create_all()
import project.init_db as init_db
from project import models

print(">>>Database created\n>>>Populating database...")
init_db.fill_db(app, db, models.Users)
print(">>>Users table populated\n>>>Creating Graphs...")
import datetime

edges = [
    '[{"id":0,"nodeA":{"id":1,"x":629.5,"y":162,"r":15,"c":"white"},"nodeB":{"id":0,"x":768.5,"y":252,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":1,"nodeA":{"id":3,"x":761.5,"y":358,"r":15,"c":"white"},"nodeB":{"id":0,"x":768.5,"y":252,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":2,"nodeA":{"id":0,"x":768.5,"y":252,"r":15,"c":"white"},"nodeB":{"id":4,"x":897.5,"y":162,"r":15,"c":"white"},"c":"black","weight":"2","directed":false},{"id":3,"nodeA":{"id":0,"x":768.5,"y":252,"r":15,"c":"white"},"nodeB":{"id":2,"x":618.5,"y":296,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":4,"nodeA":{"id":0,"x":768.5,"y":252,"r":15,"c":"white"},"nodeB":{"id":5,"x":900.5,"y":343,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":5,"nodeA":{"id":4,"x":897.5,"y":162,"r":15,"c":"white"},"nodeB":{"id":5,"x":900.5,"y":343,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":6,"nodeA":{"id":1,"x":629.5,"y":162,"r":15,"c":"white"},"nodeB":{"id":2,"x":618.5,"y":296,"r":15,"c":"white"},"c":"black","weight":"1","directed":false},{"id":7,"nodeA":{"id":3,"x":761.5,"y":358,"r":15,"c":"white"},"nodeB":{"id":5,"x":900.5,"y":343,"r":15,"c":"white"},"c":"black","weight":"56","directed":false}]',
    '[{"id":0,"nodeA":{"id":3,"x":751.5,"y":101,"r":15,"c":"white"},"nodeB":{"id":4,"x":716.5,"y":310,"r":15,"c":"white"},"c":"black","weight":"3","directed":true},{"id":1,"nodeA":{"id":3,"x":751.5,"y":101,"r":15,"c":"white"},"nodeB":{"id":0,"x":570.5,"y":138,"r":15,"c":"white"},"c":"black","weight":"3","directed":true},{"id":2,"nodeA":{"id":1,"x":574.5,"y":250,"r":15,"c":"white"},"nodeB":{"id":2,"x":656.5,"y":195,"r":15,"c":"white"},"c":"black","weight":"2","directed":true},{"id":3,"nodeA":{"id":4,"x":716.5,"y":310,"r":15,"c":"white"},"nodeB":{"id":1,"x":574.5,"y":250,"r":15,"c":"white"},"c":"black","weight":"7","directed":true},{"id":4,"nodeA":{"id":1,"x":574.5,"y":250,"r":15,"c":"white"},"nodeB":{"id":0,"x":570.5,"y":138,"r":15,"c":"white"},"c":"black","weight":"2","directed":true},{"id":5,"nodeA":{"id":2,"x":656.5,"y":195,"r":15,"c":"white"},"nodeB":{"id":3,"x":751.5,"y":101,"r":15,"c":"white"},"c":"black","weight":"1","directed":true}]',
    '[{"id":0,"nodeA":{"id":4,"x":504.5,"y":140,"r":15,"c":"white"},"nodeB":{"id":0,"x":660.5,"y":61,"r":15,"c":"white"},"c":"black","weight":"2","directed":false},{"id":1,"nodeA":{"id":0,"x":660.5,"y":61,"r":15,"c":"white"},"nodeB":{"id":1,"x":854.5,"y":52,"r":15,"c":"white"},"c":"black","weight":"1","directed":false},{"id":2,"nodeA":{"id":1,"x":854.5,"y":52,"r":15,"c":"white"},"nodeB":{"id":3,"x":1004.5,"y":116,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":3,"nodeA":{"id":3,"x":1004.5,"y":116,"r":15,"c":"white"},"nodeB":{"id":2,"x":859.5,"y":210,"r":15,"c":"white"},"c":"black","weight":"3","directed":false},{"id":4,"nodeA":{"id":4,"x":504.5,"y":140,"r":15,"c":"white"},"nodeB":{"id":5,"x":682.5,"y":197,"r":15,"c":"white"},"c":"black","weight":"2","directed":false},{"id":5,"nodeA":{"id":5,"x":682.5,"y":197,"r":15,"c":"white"},"nodeB":{"id":0,"x":660.5,"y":61,"r":15,"c":"white"},"c":"black","weight":"2","directed":false},{"id":6,"nodeA":{"id":5,"x":682.5,"y":197,"r":15,"c":"white"},"nodeB":{"id":2,"x":859.5,"y":210,"r":15,"c":"white"},"c":"black","weight":"5","directed":false},{"id":7,"nodeA":{"id":2,"x":859.5,"y":210,"r":15,"c":"white"},"nodeB":{"id":1,"x":854.5,"y":52,"r":15,"c":"white"},"c":"black","weight":"1","directed":false},{"id":8,"nodeA":{"id":1,"x":854.5,"y":52,"r":15,"c":"white"},"nodeB":{"id":5,"x":682.5,"y":197,"r":15,"c":"white"},"c":"black","weight":"7","directed":false}]',
]
nodes = [
    '[{"id":0,"x":768.5,"y":252,"r":15,"c":"white"},{"id":1,"x":629.5,"y":162,"r":15,"c":"white"},{"id":2,"x":618.5,"y":296,"r":15,"c":"white"},{"id":3,"x":761.5,"y":358,"r":15,"c":"white"},{"id":4,"x":897.5,"y":162,"r":15,"c":"white"},{"id":5,"x":900.5,"y":343,"r":15,"c":"white"}]',
    '[{"id":0,"x":570.5,"y":138,"r":15,"c":"white"},{"id":1,"x":574.5,"y":250,"r":15,"c":"white"},{"id":2,"x":656.5,"y":195,"r":15,"c":"white"},{"id":3,"x":751.5,"y":101,"r":15,"c":"white"},{"id":4,"x":716.5,"y":310,"r":15,"c":"white"}]',
    '[{"id":0,"x":660.5,"y":61,"r":15,"c":"white"},{"id":1,"x":854.5,"y":52,"r":15,"c":"white"},{"id":2,"x":859.5,"y":210,"r":15,"c":"white"},{"id":3,"x":1004.5,"y":116,"r":15,"c":"white"},{"id":4,"x":504.5,"y":140,"r":15,"c":"white"},{"id":5,"x":682.5,"y":197,"r":15,"c":"white"}]',
]
graph1 = models.Graphs(
    user_id=1,
    name="graph1",
    created_date=datetime.datetime.now(),
    edges=edges[0],
    nodes=nodes[0],
)
graph2 = models.Graphs(
    user_id=1,
    name="graph2",
    created_date=datetime.datetime.now(),
    edges=edges[1],
    nodes=nodes[1],
)
graph3 = models.Graphs(
    user_id=1,
    name="graph3",
    created_date=datetime.datetime.now(),
    edges=edges[2],
    nodes=nodes[2],
)

user2graph1 = models.Graphs(
    user_id=2,
    name="graph1",
    created_date=datetime.datetime.now(),
    edges=edges[0],
    nodes=nodes[0],
)
user2graph2 = models.Graphs(
    user_id=2,
    name="graph2",
    created_date=datetime.datetime.now(),
    edges=edges[1],
    nodes=nodes[1],
)
user2graph3 = models.Graphs(
    user_id=2,
    name="graph3",
    created_date=datetime.datetime.now(),
    edges=edges[2],
    nodes=nodes[2],
)



db.session.add_all(
    [graph1, graph2, graph3, user2graph1, user2graph2, user2graph3]
)

import random
import string

class1code = ""
class2code = ""
while class1code == class2code:
    class1code = "".join(
        random.choice(string.ascii_letters + string.digits) for x in range(5)
    )
    class2code = "".join(
        random.choice(string.ascii_letters + string.digits) for x in range(5)
    )
    class1 = models.Classes(
        teacher_id=1, name="22/23 Algorithms", students="3,6,5,4", class_code=class1code
    )
    class2 = models.Classes(
        teacher_id=2, name="Year 11 Maths", students="7,8,9", class_code=class2code
    )

db.session.add_all([class1, class2])
db.session.commit()
print(">>>Graph Database populated\n>>>Finished.")
