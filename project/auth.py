from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, login_required, logout_user
import bcrypt
from .models import Users
from .forms import LoginForm, RegisterForm
from . import db

auth = Blueprint("auth", __name__)

"""
Login route, used for registering as well
"""
@auth.route("/login", methods=["POST", "GET"])
def login():
    login_form = LoginForm()
    if login_form.submit_login.data and login_form.validate(): #on login form submission
        print("\n\nSubmitting Login_form\n\n")
        user = Users.query.filter_by(username=login_form.username.data).first()
        if not user: #no matching account
            print("No matching account")
        else: ##account has been found
            print("Account found")
            #checking whether the hash of provided passwords matches the hashed password in database
            if bcrypt.checkpw(
                login_form.password.data.encode("utf8"), user.password.encode("utf8")
            ):
                print("Success")
                login_user(user)
                if user.user_type == 1:
                    return redirect(url_for("main.home_student"))
                elif user.user_type == 2:
                    return redirect(url_for("main.home"))
                elif user.user_type == 3:
                    return redirect(url_for("main.home"))
                else:
                    print("ERROR - User doesn't have user_type")
                    return redirect(url_for("auth.login"))

    """
    Register form
    """
    register_form = RegisterForm()

    if register_form.submit_register.data and register_form.validate():
        print("\n\nSubmitting Regiter_form")
        print("Registering new account")
        user = Users.query.filter_by(username=register_form.username.data).all()
        if user: #if a record exists with the same username dont add new user
            print("User already registered under this username")
        else:
            if(register_form.teacher_check.data):
                user_type=2
            else:
                user_type=1
            #create a hash ofpassword using bcrypt with a random salt with 14 rounds
            hashedPassword = bcrypt.hashpw(
                register_form.password.data.encode("utf8"), bcrypt.gensalt(rounds=14)
            ).decode("utf8")
            #new user query
            addNewUser = Users(
                username=register_form.username.data,
                password=hashedPassword,
                email=register_form.email.data,
                user_type=user_type,
            )
            db.session.add(addNewUser)
            db.session.commit()
            return render_template(
                "login.html",
                login_form=login_form,
                register_form=register_form,
                successful_login="True",
            )
    return render_template(
        "login.html", login_form=login_form, register_form=register_form
    )

"""
Logout route, calls logout_user a flask-login function that clears session data
"""
@auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main.index"))
