from . import db
from flask_login import UserMixin


"""
Users table
"""
class Users(UserMixin, db.Model):
    id = db.Column("user_id", db.Integer, primary_key=True)
    username = db.Column(db.String(64))
    password = db.Column(db.String(128))
    email = db.Column(db.String(256))
    classes = db.Column(db.String(256))
    user_type = db.Column(db.Integer)

"""
Graphs table
"""
class Graphs(db.Model):
    id = db.Column("graph_id", db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)  # , db.ForeignKey('Users.user_id'))
    name = db.Column(db.String(128))
    created_date = db.Column(db.DateTime)
    edges = db.Column(db.String(1024))
    nodes = db.Column(db.String(1024))

"""
Classes table
"""
class Classes(db.Model):
    id = db.Column("class_id", db.Integer, primary_key=True)
    teacher_id = db.Column(db.Integer)
    name = db.Column(db.String(128))
    students = db.Column(db.String(1024))
    class_code = db.Column(db.String(16))

"""
Resutls table
"""
class Results(db.Model):
    id = db.Column("result_id", db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    homework_id = db.Column(db.Integer)
    marks = db.Column(db.String(256))
    maxMarks = db.Column(db.String(256))

"""
homeworks table
"""
class Homeworks(db.Model):
    id = db.Column("homework_id", db.Integer, primary_key=True)
    name = db.Column(db.String(512))
    class_id = db.Column(db.Integer)
    questions = db.Column(db.String(512))
    algorithms = db.Column(db.String(128))
    graphs = db.Column(db.String(512))
    setDate = db.Column(db.DateTime)
    dueDate = db.Column(db.DateTime)
    answers = db.Column(db.String(1024))
    startNodes = db.Column(db.String(256))
    endNodes = db.Column(db.String(256))
    marks = db.Column(db.String(256))

