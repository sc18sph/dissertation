/**
 * Init() used for making login form visible and register form
 * invisible on page load
 */
function init()
{
    var loginForm = document.getElementById("loginForm");
    var registerForm = document.getElementById("registerForm");
    registerForm.style.display = 'none';
    console.log("INIT");
}
/**
 * show login form and hide register form divs
 */
function showLoginForm()
{
    var loginForm = document.getElementById("loginForm");
    var registerForm = document.getElementById("registerForm");
    registerForm.style.display = 'none';
    loginForm.style.display = "inline";
}

/**
 * show login form and hide register form divs
 */
function showRegisterForm()
{
    var loginForm = document.getElementById("loginForm");
    var registerForm = document.getElementById("registerForm");
    registerForm.style.display = 'inline';
    loginForm.style.display = "none";
}