let HomeworkGraphs = [];
/**
 * When homework is loaded graph objets are created for generating correct answers
 * @param {*} nodes 
 * @param {*} edges 
 * both params passed as strings and convered to node and edge objects through graph.load
 */
function createGraphObjects(nodes,edges)
{
    for(let x = 0; x < nodes.length;x++)
    {
        let graph = new HomeworkGraph();
        graph.load(nodes[x],edges[x]);
        HomeworkGraphs.push(graph);

    }  
}
/**
 * Called to draw the current graph to the canvas on attemptHomework.html
 * similar to function found in javascript.js
 * @param {*} graph 
 */
function redrawCanvas(graph) {
    var canvas = document.getElementById("homeworkCanvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
  
    for (let edgeLoop = 0; edgeLoop < graph.edges.length; edgeLoop++) {
      graph.edges[edgeLoop].draw(); //draw edges
    }
  
    for (let nodeLoop = 0; nodeLoop < graph.nodes.length; nodeLoop++) {
        graph.nodes[nodeLoop].c = "white";
        graph.nodes[nodeLoop].draw(); //draw nodes
    }
    
  
  }

/**
 * HomeworkGraph class, stores the graphs as objects
 * similar to graph class found in Javascript.js
 */
class HomeworkGraph {
    constructor() {
      this.nodes = [];
      this.edges = [];
      this.name = "";
      this.id = 0;
    }
    /**tableCreate
   * getMatrix is used for converting the graph object into a 2d array
   * that represents an adjacency matrix
   */
  getMatrix() {
    var matrix = [];
    //create a 2d array with default values of 0
    for (let x = 0; x < this.nodes.length; x++) {
      matrix.push(new Array(this.nodes.length).fill(0));
    }

    //iterate through edges in graph and add weight to array
    //check if directed and if so dont make symmetrical
    for (let edgeLoop = 0; edgeLoop < this.edges.length; edgeLoop++) {
      var edge = this.edges[edgeLoop];
      var Node1 = edge.nodeA.id;
      var Node2 = edge.nodeB.id;
      if (edge.weight) {
        var weight = edge.weight;
      } else {
        var weight = 0;
      }
      if (edge.directed) {
        //if it is directed then only [node1][node2] affected
        matrix[Node1][Node2] = Number(weight);
      } else {
        //if not directed then both directions on matrix ammended
        matrix[Node1][Node2] = Number(weight);
        matrix[Node2][Node1] = Number(weight);
      }
    }

    return matrix;
  }
    /**
     * used for converting string of nodes and edges into node and edge objects stored in graph object
     * @param {*} nodes 
     * @param {*} edges 
     */
    load(nodes,edges) {
      var JSON_Nodes = JSON.parse(nodes);
      var JSON_Edges = JSON.parse(edges);
      for (var node of JSON_Nodes) {
        //loop through edges in JSON object and create new nodes from them
        var newNode = new HomeworkNode();
        newNode.id = node.id;
        newNode.x = node.x;
        newNode.y = node.y;
        newNode.r = node.r;
        newNode.c = node.c;
        this.nodes.push(newNode);
      }
      for (var edge of JSON_Edges) {
        //loop through edges in JSON object and create new edges from them
        var newEdge = new HomeworkEdge();
        newEdge.id = edge.id;
        newEdge.nodeA = this.nodes[edge.nodeA.id];
        newEdge.nodeB = this.nodes[edge.nodeB.id];
        newEdge.weight = edge.weight;
        newEdge.c = edge.c;
        newEdge.directed = edge.directed;
        this.edges.push(newEdge);
      }
    }
  }
  
  /**
   * Node class
   * Contains ID, xCoord, yCoord, size and colour
   * Include Functions:
   *      - draw() - which when called gets canvas element from webpage and creates a circle and draws
   *  similar to Node class found in Javascript.js
   */
  class HomeworkNode {
    constructor(xCoord, yCoord) {
      this.id = null;
      this.x = xCoord;
      this.y = yCoord;
      this.r = 15;
      this.c = "white";
    }
    draw() {
      var canvas = document.getElementById("homeworkCanvas");
      var context = canvas.getContext("2d");
      //Drawing circle with black outline
      context.beginPath();
      context.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
      context.fillStyle = this.c;
      context.fill();
      context.lineWidth = "1";
      context.strokeStyle = "black";
      context.stroke();
  
      //drawing ID number within node
      context = canvas.getContext("2d");
      context.beginPath();
      context.font = "15px Georgia";
      context.textAlign = "center";
      context.textBaseline = "middle";
      context.fillStyle = "red";
      context.fillText(this.id, this.x, this.y);
      context.fill();
    }
  }
  
  /**
   * Edge Class
   * Contains ID, Parent nodes (A&B) and colour
   * Draw function called on edge object and will draw to canvas
   * similar to edge class found in javasciprt.js
   */
  class HomeworkEdge {
    constructor(nodeA, nodeB, weight) {
      this.id = null;
      this.nodeA = nodeA;
      this.nodeB = nodeB;
      this.c = "black";
  
      this.weight = weight;
      this.directed = false;
    }
    draw() {
      console.log("Directed = " , this.directed);
      var canvas = document.getElementById("homeworkCanvas");
      var context = canvas.getContext("2d");
      context.strokeStyle = this.c;
      context.lineWidth = 3;
      context.beginPath();
      context.moveTo(this.nodeA.x, this.nodeA.y); //start at [x,y] of node 1
      context.lineTo(this.nodeB.x, this.nodeB.y); //end at [x,y] of node 2
    
      //If the edge is directed then an arrow needs to be drawn
      if (this.directed) {
        var angle = Math.atan2(
          this.nodeB.y - this.nodeA.y,
          this.nodeB.x - this.nodeA.x
        );
          //below is arrow head creation
        var headLength = 30;
        var circleEdgeX = this.nodeB.x - this.nodeB.r * Math.cos(angle);
        var circleEdgeY = this.nodeB.y - this.nodeB.r * Math.sin(angle);

        context.moveTo(circleEdgeX, circleEdgeY);
        context.lineTo(
          this.nodeB.x - headLength * Math.cos(angle - Math.PI / 12),
          this.nodeB.y - headLength * Math.sin(angle - Math.PI / 12)
        );
        context.moveTo(circleEdgeX, circleEdgeY);
        context.lineTo(
          this.nodeB.x - headLength * Math.cos(angle + Math.PI / 12),
          this.nodeB.y - headLength * Math.sin(angle + Math.PI / 12)
        );
      }
  
      context.stroke(); //draw line for the edge
      if (this.weight) {
        var xCoord = (this.nodeA.x + this.nodeB.x) / 2;
        var yCoord = (this.nodeA.y + this.nodeB.y) / 2;
        //draw box for weight
        context.canvas.getContext("2d");
        context.beginPath();
        context.fillStyle = "#f8f8f8";
        context.fillRect(xCoord - 10, yCoord - 10, 20, 20);
        context.stroke();
        //drawing weight number of edge
        context = canvas.getContext("2d");
        context.beginPath();
        context.font = "15px Georgia";
        context.textAlign = "center";
        context.textBaseline = "middle";
        context.fillStyle = "red";
        context.fillText(this.weight, xCoord, yCoord);
        context.fill();
      }
    }
  }


/**
 * Function used to compute the weight of the shortest path as well as the shortest path
 * 
 * @param {*} startNode startNode integer
 * @param {*} endNode  endNode integer
 * @param {*} adjMatrix  adjacency matrix representing the graph
 * @returns 
 *      - weight of path (integer)
 *      - shortest path (comma seperated numbers representing the nodes)
 */ 
function Dijkstra(startNode, endNode, adjMatrix) {
  let numberOfNodes = adjMatrix.length; //number of nodes in graph
  let distanceArray = new Array(numberOfNodes).fill(Infinity); //array of distances from start node, init as Inifinity (as per DJs)
  let previousArray = new Array(numberOfNodes).fill(null); //stores previous node for each node on the shortest path
  let unvisitedArray = new Set(Array.from({ length: numberOfNodes }, (_, index) => index)); //set to track which nodes are visited
  distanceArray[startNode] = 0; //distance from start to self = 0
  while (unvisitedArray.size > 0) {//while all node have no been visited
    let currentNode = Array.from(unvisitedArray).reduce((a, b) => distanceArray[a] < distanceArray[b] ? a : b); //find the node w/ shortest distance from start node
    if (currentNode === endNode) //if we are at the end node then break the loop
    {
      break;
    }
    unvisitedArray.delete(currentNode); //mark the current node as visited by removing from the unvisited array

    for (let checkNode = 0; checkNode < numberOfNodes; checkNode++) { //for all nodes in the graph
      if (adjMatrix[currentNode][checkNode] !== 0 && unvisitedArray.has(checkNode)) //if there is an edge between current node and checkNode  and is unvisited
      {
        let tmpDistance = distanceArray[currentNode] + adjMatrix[currentNode][checkNode]; //temporary distance to checkNode
        if (tmpDistance < distanceArray[checkNode])  //if the new tmpDistance is less than previously saved distance then need to overwrite
        {
          distanceArray[checkNode] = tmpDistance; //new distance for checkNode = tmpDistance
          previousArray[checkNode] = currentNode; //set the previous node to the current node
        }
      }
    }
  }

  let shortestPathNodes = [endNode];
  let endOfPath = endNode;
  while (previousArray[endOfPath] !== null) //iterate through the previous array to consturct the shortest path from start to finish
  {
    shortestPathNodes.unshift(previousArray[endOfPath]); //add previous node to the front of the path
    endOfPath = previousArray[endOfPath]; //go back 1 more node
  }
  //return the shortest path array as a comma seperated string and the distance to end node from distance Array
  return [shortestPathNodes.join(","), distanceArray[endNode]];
}
  

  
/**
 *  inspired from https://www.geeksforgeeks.org/prims-mst-for-adjacency-list-representation-greedy-algo-6/
          with changes to suit my representation of graphs,nodes and edges
 * @param {*} adjMatrix 
 * used for computing the weight of the minimum spanning tree of the graph for marking
 */
  function computeMSTWeight(adjMatrix) 
  {
    let n = adjMatrix.length; //number of nodes
    let edges = []; //2d array used to store all edges with x/y node and weight 
    let mstWeight = 0; //the result that we return at the end
    //create an array of parent nodes (every node in graph 0...n-1)
    let parent = [];
    for (let i = 0; i < n; i++) {
      parent.push(i);
    }
    // loop through every element in the 2d adjaceny matrix
    // if there is a weight (i.e. the element is not 0 then push the x and y coord with the weight to an array of edges)
    for (let x = 0; x < n; x++) 
    {
      for (let y = x + 1; y < n; y++) 
      {
        if (adjMatrix[x][y] !== 0) 
        {
          edges.push([x, y, adjMatrix[x][y]]);  //push x,y and weight to edges
        }
      }
    }
    // sort the edges in the 2d array from smallest weight to largest weight
    // https://www.w3schools.com/js/tryit.asp?filename=tryjs_array_sort2
    edges.sort((a, b) => a[2] - b[2]);
    // Find the parent of the node recursively 
    function findParent(node) {
      if (parent[node] === node) {
        return node;
      }
      parent[node] = findParent(parent[node]);
      return parent[node];
    }
  
    //for each edges in the 2d array
    for (let edgeLoop = 0; edgeLoop < edges.length; edgeLoop++) 
    {
      let parentXNode = findParent(edges[edgeLoop][0]); //find parent node of X
      let parentYNode = findParent(edges[edgeLoop][1]); //find parent node of Y
  
      if (parentXNode !== parentYNode) { //if the parent nodes are not the same then adding this edges doesnt create a cycle
        mstWeight += edges[edgeLoop][2]; //add the weight of the edge of the overall MST weight
        parent[parentYNode] = parentXNode; //make the XNode the parent of the Y node as has changed from adding new edge
      }
    }
  
    return mstWeight; //return the integer value of the MST weight
  }