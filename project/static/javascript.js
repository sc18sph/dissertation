/**
 * State Class
 * Saves the current state of the page
 * Currently selected node ID
 * currently selected toolbar button
 */
class State {
  constructor() {
    this.selected = null;
    this.selected2 = null;
    this.toolbarButtonSelected = null;
  }
}

/**
 * Graph Class
 * Stores information about the created or loaded graph
 * Includes functions:
 *      - getMatrix() - creates a 2d array of integers
 *      - getJSON()/save()   - creates a JSON object used for storing graph in database
 *      - load() - peforms SQL SELECT query to get stored JSON object for both nodes & edges and clears and updates graph and redraws
 *
 */

class Graph {
  constructor() {
    this.nodes = [];
    this.edges = [];
    this.name = "";
    this.id = 0;
  }
  /**tableCreate
   * getMatrix is used for converting the graph object into a 2d array
   * that represents an adjacency matrix
   */
  getMatrix() {
    var matrix = [];
    //create a 2d array with default values of 0
    for (let x = 0; x < this.nodes.length; x++) {
      matrix.push(new Array(this.nodes.length).fill(0));
    }

    //iterate through edges in graph and add weight to array
    //check if directed and if so dont make symmetrical
    for (let edgeLoop = 0; edgeLoop < this.edges.length; edgeLoop++) {
      var edge = this.edges[edgeLoop];
      var Node1 = edge.nodeA.id;
      var Node2 = edge.nodeB.id;
      if (edge.weight) {
        var weight = edge.weight;
      } else {
        var weight = 0;
      }
      if (edge.directed) {
        //if it is directed then only [node1][node2] affected
        matrix[Node1][Node2] = Number(weight);
      } else {
        //if not directed then both directions on matrix ammended
        matrix[Node1][Node2] = Number(weight);
        matrix[Node2][Node1] = Number(weight);
      }
    }

    tableCreate(matrix);
    return matrix;
  }
  /**
   * getJSON() converts the node and graph arrays to 2 seperate JSON objects
   */
  getJSON() {
    var nodesStr = JSON.stringify(graph.nodes);
    var edgesStr = JSON.stringify(graph.edges);
    var JSON_Nodes = JSON.parse(nodesStr);
    var JSON_Edges = JSON.parse(edgesStr);
  }
  /**
   * Returns a JSON object of nodes and edges used for saving in database
   */
  save() {
    console.log("\n\nSave() function returns:\n[0]=",this.nodes.toString(),"\n[1]=",this.edges.toString());
    return [JSON.stringify(this.nodes),JSON.stringify(this.edges)];
  }
  /**
   * 
   * @param {*} nodes 
   * @param {*} edges 
   * load() is used for loading a graph from 2 saved strings (edges and nodes) from
   * the database and converting the strings into a graph object
   */
  load(nodes,edges) {
    this.nodes = [];
    this.edges = [];

    var JSON_Nodes = JSON.parse(nodes);
    var JSON_Edges = JSON.parse(edges);

    for (var node of JSON_Nodes) {
      //loop through edges in JSON object and create new nodes from them
      var newNode = new Node();
      newNode.id = node.id;
      newNode.x = node.x;
      newNode.y = node.y;
      newNode.r = node.r;
      newNode.c = node.c;
      
      this.nodes.push(newNode);
    }
    for (var edge of JSON_Edges) {
      //loop through edges in JSON object and create new edges from them
      var newEdge = new Edge();
      newEdge.id = edge.id;
      newEdge.nodeA = this.nodes[edge.nodeA.id];
      newEdge.nodeB = this.nodes[edge.nodeB.id];
      newEdge.weight = edge.weight;
      newEdge.c = edge.c;
      newEdge.directed = edge.directed;
      this.edges.push(newEdge);
    }
    redrawCanvas(); //once new graph has been created draw new graph to canvas
  }
}

//create new state and graph object on page load
var state = new State();
var graph = new Graph();
/**
 * Node class
 * Contains ID, xCoord, yCoord, size and colour
 * Include Functions:
 *      - draw() - which when called gets canvas element from webpage and creates a circle and draws
 *
 */
class Node {
  constructor(xCoord, yCoord) {
    this.id = graph.nodes.length;
    this.x = xCoord;
    this.y = yCoord;
    this.r = 15;
    this.c = "white";
  }
  draw() {
    var canvas = document.getElementById("graphCanvas");
    var context = canvas.getContext("2d");
    //Drawing circle with black outline
    context.beginPath();
    context.arc(this.x, this.y, this.r, 0, 2 * Math.PI); //draw circle
    context.fillStyle = this.c; //fill colour
    context.fill();
    context.lineWidth = "1"; //width of border
    context.strokeStyle = "black"; //border colour
    context.stroke(); //draw

    //drawing ID number within node
    context = canvas.getContext("2d");
    context.beginPath();
    context.font = "15px Georgia"; //font
    context.textAlign = "center"; //position in circle
    context.textBaseline = "middle"; //position in circle
    context.fillStyle = "red"; //font colour
    context.fillText(this.id, this.x, this.y); //draw text
    context.fill();
  }
}

/**
 * Edge Class
 * Contains ID, Parent nodes (A&B) and colour
 * Draw function called on edge object and will draw to canvas
 */
class Edge {
  constructor(nodeA, nodeB, weight) {
    this.id = graph.edges.length;
    this.nodeA = nodeA;
    this.nodeB = nodeB;
    this.c = "black";

    this.weight = weight;
    this.directed = false;
  }
  draw() {
    var canvas = document.getElementById("graphCanvas");
    var context = canvas.getContext("2d");
    context.strokeStyle = this.c; //edge colour
    context.lineWidth = 3; //edge width
    context.beginPath();
    context.moveTo(this.nodeA.x, this.nodeA.y); //start at [x,y] of node 1
    context.lineTo(this.nodeB.x, this.nodeB.y); //end at [x,y] of node 2
    //If the edge is directed then need to draw an arrow head
    if (this.directed) {
      var angle = Math.atan2(
        this.nodeB.y - this.nodeA.y,
        this.nodeB.x - this.nodeA.x
      );
      var headLength = 30; //size of arrow head

      //calculating the coordinates of the edge of the node where arrow head tip is
      var circleEdgeX = this.nodeB.x - this.nodeB.r * Math.cos(angle);
      var circleEdgeY = this.nodeB.y - this.nodeB.r * Math.sin(angle);

      context.moveTo(circleEdgeX, circleEdgeY);
      //draw one half of arrow head
      context.lineTo(
        this.nodeB.x - headLength * Math.cos(angle - Math.PI / 12),
        this.nodeB.y - headLength * Math.sin(angle - Math.PI / 12)
      );
      //move back to edge of node
      context.moveTo(circleEdgeX, circleEdgeY);
      //draw second half of arrow head
      context.lineTo(
        this.nodeB.x - headLength * Math.cos(angle + Math.PI / 12),
        this.nodeB.y - headLength * Math.sin(angle + Math.PI / 12)
      );
    }

    context.stroke(); //draw edge
    if (this.weight) {
      var xCoord = (this.nodeA.x + this.nodeB.x) / 2;
      var yCoord = (this.nodeA.y + this.nodeB.y) / 2;
      //draw box for weight
      context.canvas.getContext("2d"); 
      context.beginPath();
      context.fillStyle = "#f8f8f8"; //edge colour
      context.fillRect(xCoord - 10, yCoord - 10, 20, 20);
      context.stroke();
      //drawing weight number of edge
      context = canvas.getContext("2d");
      context.beginPath();
      context.font = "15px Georgia"; //weight number font
      context.textAlign = "center"; //weight number position
      context.textBaseline = "middle";
      context.fillStyle = "red"; //weight nuber font colour
      context.fillText(this.weight, xCoord, yCoord); //draw text
      context.fill();
    }
  }
}

/**
 *
 * @param {*} xCoord X Coordinate of where to generate Node
 * @param {*} yCoord Y Coordinate of where to generate Node
 *
 * Create new Node object
 * Push to Node stack
 * call draw() function
 */
function createNode(xCoord, yCoord) {
  //alert("CreateNode Running")
  var newNode = new Node(xCoord, yCoord); //create new node
  graph.nodes.push(newNode); //push node onto end of Nodes stack
  newNode.draw(); //call draw function on new node
  redrawCanvas(); //redraw canvas
}

/**
 * 
 * @param {*} nodeA NodeA = starting node
 * @param {*} nodeB NodeB = end node
 * @param {*} weight weight = weight of the edge
 * create a new edge object 
 * push the edge to stack
 * call draw() function on edge
 */
function createEdge(nodeA, nodeB, weight) {
  var newEdge = new Edge(nodeA, nodeB);
  //if there is a weight then assign it
  if (weight) {
    newEdge.weight = weight;
  } else { //else leave as 0
    newEdge.weight = 0;
  }
  //If the directedToggle button is selected then set that directed property
  //of the edge to true
  if (document.getElementById("directedToggle").checked) {
    newEdge.directed = true;
  }
  graph.edges.push(newEdge);
  newEdge.draw();
  //reset which nodes are selected
  state.selected = null;
  state.selected2 = null;
  redrawCanvas();
}


/**
 * moveNodes is designed to detect whether draging has occured by measuring whether the 
 * one the mouse has been lifted if it has travelled a certain distance
 * if it has travelled a certain distance and a node has been selected then the 
 * nodes coordinates are changed to reflect the change in mouse position and
 * the graph is redrawn to the canvas 
 */
function moveNodes() {
  var canvas = document.getElementById("graphCanvas");
  var context = canvas.getContext("2d");
  const delta = 20; //delta used so drag has to be over a certain threshold before drag is detected to avoid incorrect inputs
  let startX;
  let startY;
  const rect = canvas.getBoundingClientRect();
  //when the mouse button is clicked the x and y coordinates of the click location are recorded
  canvas.addEventListener("mousedown", function (event) {
    startX = event.pageX - rect.left;
    startY = event.pageY - rect.top;
    document.body.style.cursor = "move";
  });
  //when the mouse button has been released, if the movement is over the set delta then coordinates are changed, else nothing
  canvas.addEventListener("mouseup", function (event) {
    document.body.style.cursor = "default";
    const diffX = Math.abs(event.pageX - rect.left - startX);
    const diffY = Math.abs(event.pageY - rect.top - startY);
    if (diffX < delta && diffY < delta) {
      //this is a click, do nothing
    } else {
      //this is a drag
      if (state.selected != null) {
        graph.nodes[state.selected].x = event.pageX - rect.left;
        graph.nodes[state.selected].y = event.pageY - rect.top;
        state.selected = null;
        redrawCanvas();
      }
    }
  });
}

/**
 * windowResize()
 *      Deals with browser resizing and changes the size of the canvas to fit
 *      Once resized the canvas is redrawn
 */
function windowResize() {
  var canvas = document.getElementById("graphCanvas");
  var context = canvas.getContext("2d");
  context.canvas.width = 0.9 * window.innerWidth; //set canvas to 90% width of screen
  context.canvas.height = 0.4 * window.innerHeight; //set canvas to 40% height of screen
  redrawCanvas(); //once reset size, redraw the canvas
}

/**
 * init()
 *      Initialises the size of the canvas relative to the size of the browser window
 *      Calls moveNodes() which initialises a listener event for dragging nodes to edit position
 *      Calls addWeight() which initialises a listener event for creating pop up for adding weights
 *                          to newly created edges
 *      Creates a listener event for when the mouse is clicked which deals with whether to create an
 *                          edge and whether a weight will be added to newly created edge
 *      Finally, ensures windowResize() is called when the window is resized
 */

function init() {

  var canvas = document.getElementById("graphCanvas"); //get canvas
  var context = canvas.getContext("2d");
  context.canvas.width = 0.9 * window.innerWidth; //set canvas width
  context.canvas.height = 0.6 * window.innerHeight; //set canvas height
  moveNodes(); //init move node listener
  var toolbar = document.getElementById("toolbar");

  $(".close").click(function () { //if close button on overlay clicked then hide overlay 
    $(".overlay").hide();
  });

  addWeight(); //init listener for creating pop up
  //listener for mouse click
  canvas.addEventListener("mousedown", function (e) {
    getCursorPosition(canvas, e);
  });
  window.onresize = windowResize; //when window is resized called windowResize function 
}

/**
 * JQuery managing button click
 * Hiding / Showing form
 * Reading form data (weight)
 */
function addWeight() {
  var weight = null;
  const form = document.getElementById("weightForm");
  form.addEventListener("submit", (e) => {
    e.preventDefault();
    var formData = new FormData(form);
    for (const pair of formData.entries()) {
      weight = pair[1]; //assign the input weight on form to weight
    }
    $(".overlay").hide(); //hide the weight form overlay
    if (Number.isInteger(Number(weight)) == true && weight>0) { //check theinput is a number above 0
      //document.getElementById("fname").value = "";
      createEdge( //if valid input then create a new edge with the corresponding weight
        graph.nodes[state.selected],
        graph.nodes[state.selected2],
        weight
      );
    } else { //if not a number above 0 then inform user through alert
      alert("Weight must be an iteger [" + weight + "] is not a valid integer");
    }
  });
}

/**
 *
 * @param {*} canvas Canvas element
 * @param {*} event Event Element
 * getCursorPosition is called when the mouse has been clicked
 * It is used to detect whether a node has been clicked on
 * If no node is currently selected it will set "selected" to the clicked node
 * If a node is already selected and a new node is clicked then it will check whether the user 
 *    has selected to create a new edge, if so it will the check if the user wants the edge
 *    to have a weight, given these user options it will create an edge to their specification
 */
function getCursorPosition(canvas, event) {
  let hit = false; 
  const rect = canvas.getBoundingClientRect();
  const x = event.clientX - rect.left; //x coord on canvas
  const y = event.clientY - rect.top; //y coord on canvas
  //Loop used for checking if a node has been clicked on
  for (let nodeLoop = 0; nodeLoop < graph.nodes.length; nodeLoop++) {
    xCoord = graph.nodes[nodeLoop].x; //node[x] x coord
    yCoord = graph.nodes[nodeLoop].y; //node[x] y coord

    //if the clicked x and y is within node[x] x and y then hit = true as node has been hit
    if (
      x > xCoord - graph.nodes[nodeLoop].r &&
      x < xCoord + graph.nodes[nodeLoop].r &&
      y > yCoord - graph.nodes[nodeLoop].r &&
      y < yCoord + graph.nodes[nodeLoop].r
    ) {
      hit = true; //node has been hit
      if (state.selected == null) { //if no node is currently selected then set hit node to selected
        state.selected = graph.nodes[nodeLoop].id; 
        redrawCanvas(); //redraw canvas
      } else { //if a node is already selected
        if (printSelectedButton() == 1) { //check wether create EdgeButton is selected
          if (!document.getElementById("weightToggle").checked) { //If user wants edge to have NO weight
            createEdge( //create new edge object
              graph.nodes[state.selected],
              graph.nodes[graph.nodes[nodeLoop].id]
            );
          } else { //if user wants edge to have a weight
            state.selected2 = graph.nodes[nodeLoop].id; //set the 2nd selected node in state
            $(".overlay").show(); //show the weight popup
            //select the input box automatically
            window.setTimeout(
              () => document.getElementById("fname").focus(),
              0
            ); //focus on the input box automatically
          }
        }
      }
    }
  }
  //if blank area clicked and createNodeButton is selected then create a new node
  if (hit == false && printSelectedButton() == 0) {
    state.selected = null;
    createNode(x, y);
  }
}
/**
 * delete the current graph and reset the canvas
 */
function clearCanvas() {
  var canvas = document.getElementById("graphCanvas");
  var context = canvas.getContext("2d");

  context.clearRect(0, 0, canvas.width, canvas.height); //clear canvas
  var clearButton = document.getElementById("clearButton"); 
  clearButton.removeAttribute("style");

  graph.nodes = []; //clear the current graph objects saved data
  graph.edges = []; //clear the current graph objects saved data
}

/**
 * redrawCanvas() is used redrawing all current data onto the canvas when there is a change
 * It works by first clearing the canvas and then iteratively redrawing each node and edge
 * until completion 
 */
function redrawCanvas() {
  var canvas = document.getElementById("graphCanvas"); //get canvas
  var context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height); //clear canvas

  for (let edgeLoop = 0; edgeLoop < graph.edges.length; edgeLoop++) { //draw each edge in graph
    graph.edges[edgeLoop].draw();
  }

  for (let nodeLoop = 0; nodeLoop < graph.nodes.length; nodeLoop++) {//draw each node in graph
    if (nodeLoop == state.selected) { //preserve selected node options
      graph.nodes[nodeLoop].c = "green";//if selected then colour = green
    } else {
      graph.nodes[nodeLoop].c = "white"; //if not selected then colour = white
    }

    graph.nodes[nodeLoop].draw();
  }
  createStartNodeDropdown(); //update dropdown to show correct number of nodes
  

}
//used to save the graph data to fields in html that are used by Flask to save graph to database
function addHiddenSaveFields(){
  document.getElementById('nodes_hidden').value=graph.save()[0]; //set the node data to hidden field
  document.getElementById('edges_hidden').value=graph.save()[1]; //set the edge data to hidden field
}

/**
 * Similar use to redrawcanvas but specialised for the algorithm visualisation
 */
function algorithmRedrawCanvas() {
  var canvas = document.getElementById("graphCanvas");
  var context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height); //clear canvas
  for (let edgeLoop = 0; edgeLoop < graph.edges.length; edgeLoop++) { //redraw edges
    graph.edges[edgeLoop].draw();
  }
  for (let nodeLoop = 0; nodeLoop < graph.nodes.length; nodeLoop++) { //redraw nodes
    graph.nodes[nodeLoop].draw();
  }
}

/**
 * used in detecting which button is currently selected
 * returns the index of the button in the toolbarbuttons array and uses jquery
 * to check which has a class of active (currently selected)
 */
function printSelectedButton() {
  var toolbarButtons = ["createNodeButton", "createEdgeButton", "deleteButton"];
  for (let x = 0; x < toolbarButtons.length; x++) {
    if ($(document.getElementById(toolbarButtons[x])).hasClass("active")) {
      return x; //return index of currently selected button
    }
  }
}

/**
 * deleteNode() test whether a node is currently selected, if so then delete all edges 
 * that connect to that node and delete the node itself.
 * Once deleted all associating data to node then redraw canvas without node or connecting edges
 */
function deleteNode() {
  if (state.selected != null) {
    for (let edgeLoop = 0; edgeLoop < graph.edges.length; edgeLoop++) {
      if (
        graph.edges[edgeLoop].nodeA.id == state.selected ||
        graph.edges[edgeLoop].nodeB.id == state.selected
      ) {
        graph.edges.splice(edgeLoop, 1);
        edgeLoop--;
      }
    }

    graph.nodes.splice(state.selected, 1);

    state.selected = null;
    for (let x = 0; x < graph.nodes.length; x++) {
      graph.nodes[x].id = x;
    }
    redrawCanvas();
  }
}
/**
 * Used for debugging to print all nodes in graph to console
 */
function printNodes() {
  for (let x = 0; x < graph.nodes.length; x++) {
    console.log(graph.nodes[x]);
  }
}

/**
 * used for debugging to print all edges in graph to console
 */
function printEdges() {
  for (let x = 0; x < graph.edges.length; x++) {
    console.log(graph.edges[x]);
  }
}

/**
 * used for debugging to print both nodes and edges to console
 */
function printSavedGraph() {
  var nodesStr = JSON.stringify(graph.nodes);
  var edgesStr = JSON.stringify(graph.edges);
  var JSON_Nodes = JSON.parse(nodesStr);
  var JSON_Edges = JSON.parse(edgesStr);

  console.log(nodesStr);
  console.log(edgesStr);
}

/**
 * Change the text of algorithm select button to reflect which algorithm has been chosen 
 */
function showAlgorithm(item,algo) {
  var button = document.getElementById("algorithmSelectionButton");
  if(algo == 'dj')
  {
    document.getElementById("kruskalsButtonDiv").style.display="none";
    document.getElementById("startNodeDiv").style.display="block";
  }else{
    document.getElementById("kruskalsButtonDiv").style.display="block";
    document.getElementById("startNodeDiv").style.display="none";

  }
  button.innerHTML = item.innerHTML + '<span class="caret"></span>';
}


/**
 * used for creating an adjacency matrix of current graph
 * @param {*} matrix 
 */
function tableCreate(matrix) {
  var matrixTable = document.getElementById("matrixTable");
  //init start of table
  let table = `
    <table class="table table-bordered">
    <thead>
    <th scope="col">#</th>
    
    `;
  for (let x = 0; x < matrix.length; x++) { //add a new column header for every node
    table += '<th scope="col">' + x + "</th>";
  }
  table += "</thead><tbody>";

  for (let x = 0; x < matrix.length; x++) { 
    table += '<tr><th scope="row">' + x + "</th>"; //add a row header for every node
    for (let y = 0; y < matrix[0].length; y++) {
      table += "<td>" + matrix[x][y] + "</td>"; //add the weight between node x and node y to [x][y] in table
    }
    table += "</tr>";
  }
  table += "</tbody>";

  matrixTable.innerHTML = table; //write table to the DIV
}

/**
 * startNodeDropdown is a drop down selection box that is generated by counting the number of nodes and displaying them as options
 */
function createStartNodeDropdown() {
  var startNodeDropdown = document.getElementById("startNodeDropdown");
  var dropdownHTML = "";
  for (let x = 0; x < graph.nodes.length; x++) {
    dropdownHTML +=
      '<li><button onclick="setCurrentNode(' +
      x +
      ')" type="button" class="dropdown-item btn btn-default" style="width:100%; text-align: center;">' +
      x +
      "</button></li>";
  }

  startNodeDropdown.innerHTML = dropdownHTML;
}
/**
 * once a start node has been selected then it is written to the screen as a renubder 
 */
function setCurrentNode(nodeNumber) {
  var htmlText = document.getElementById("currentlySelectedNode");
  htmlText.innerHTML =
    "Currently Selected Node: " +
    nodeNumber +
    `<br><button onclick="runDJ()" type="button" class="btn btn-default">Run Dijkstra's</button>`;
  sessionStorage.startNode = nodeNumber;
}


/**
 * 
 * @param {*} distance 
 * @param {*} visited 
 * table is generated dynamically to show the process of dijkstas algorithm and how
 * the distance array and visisted array are used and affected thruogh its execution
 */
function showAlgorithmTable(distance, visited) {
  var div = document.getElementById("algorithmSteps");
  var table = `<table class="table table-bordered" style="width: 90%; margin:auto;">
    <tr>
        <th scope="col">Node</th>    
    `;
  var distanceRow = '<tr><th scope="col">Distance</th>';
  var visitedRow = '<tr><th scope="col">Visited</th>';
  for (let x = 0; x < distance.length; x++) {
    table += '<th scope="col">' + x + "</th>";
    if (visited[x] == true) {
      visitedRow += '<td class="bg-success">' + visited[x] + "</td>";
      distanceRow += '<td class="bg-success">' + distance[x] + "</td>";
    } else {
      visitedRow += "<td>" + visited[x] + "</td>";
      distanceRow += "<td>" + distance[x] + "</td>";
    }
  }
  table +=
    "</tr><tbody>" +
    distanceRow +
    "</tr>" +
    visitedRow +
    "</tr></tbody></table>";
  div.innerHTML = table;
}

/**
 * Dijkstras algorithm run through
 */
let steps = [];
let dist_show = [];
let visited_show = [];

let currentStep = 0;
/**
 * @param play if true the algorithm visualisation to run automatically
 * if false then algorithm visualisation will run next step whne next button is pressed  
 */
function algorithmControls_next(play) {
  /*
    Instructions:
        - CRE_DIST_VISI_LEN_<length>
        - SET_DIST_ALL_INF
        - SET_VISI_ALL_false
        - SET_DIST_<node>_<value>
        - MIN_DIST_<node>
        - SET_VISI_<node>_true
        - CHE_DIST_node
    */
  stepText = document.getElementById("currentStepDiv");
  var instruction = steps[currentStep].split("|")[0];
  if (instruction.startsWith("CRE_DIST_VISI_LEN_")) {
    for (var x = 0;x <parseInt(instruction.substring(instruction.lastIndexOf("_") + 1,instruction.length));x++) 
    {
      dist_show.push("_");
      visited_show.push("_");
    }
  }

  if (instruction == "SET_DIST_ALL_INF") {
    dist_show.fill("inf");
  } else if (instruction == "SET_VISI_ALL_false") {
    visited_show.fill(false);
  } else if (instruction.startsWith("SET_DIST_")) {
    dist_show[parseInt(instruction.split("_")[2])] = parseInt(
      instruction.split("_")[3]
    );
  } else if (instruction.startsWith("SET_VISI_")) {
    visited_show[parseInt(instruction.split("_")[2])] = true;
    graph.nodes[parseInt(instruction.split("_")[2])].c = "green";
  } else if (instruction.startsWith("CHE_DIST_")) {
    for (let x = 0; x < graph.nodes.length; x++) {
      if (graph.nodes[x].c != "green") {
        graph.nodes[x].c = "white";
      }
    }
    graph.nodes[parseInt(instruction.split("_")[2])].c = "pink";
  } else if (instruction.startsWith("MIN_DIST_")) {
    for (let x = 0; x < graph.nodes.length; x++) {
      if (graph.nodes[x].c != "green" && graph.nodes[x].c != "pink") {
        graph.nodes[x].c = "white";
      }
    }
  }

  document.getElementById("algorithmStepsText").innerHTML =
    "Instruction: " + steps[currentStep].split("|")[1];
  currentStep++;
  showAlgorithmTable(dist_show, visited_show);
  algorithmRedrawCanvas();
  if (play && instruction != "FINISH") {
    setTimeout(algorithmControls_next, 1000, true); //pause between lines executed to user can see process
  }
  if (instruction == "FINISH") {
    currentStep = 0;
    dist_show = [];
    visited_show = [];
    steps = [];
    document.getElementById("algorithmStepsText").innerHTML =
      "Instruction: FINISHED";
  }
}
/**
 * execution of dijkstras algorithm on graph
 */
function runDJ() {
  document.getElementById("currentlySelectedNode").innerHTML = "<h4>To run a visualisation choose play or next</h4>";
  document.getElementById("DJsControlButtons").style.display="block";
  document.getElementById("KruskalsControlButtons").style.display="none";

  dijkstra(
    graph.getMatrix(),
    sessionStorage.getItem("startNode"),
    graph.getMatrix().length
  );
}

function minDistance(dist, visited, nodeCount) {
  // Initialize min value
  let min = Number.MAX_VALUE;
  let min_index = -1;

  for (let v = 0; v < nodeCount; v++) {
    if (visited[v] == false && dist[v] != Number.MAX_VALUE) {
      steps.push("CHE_DIST_" + v + "|Check the distance to node " + v);
      if (dist[v] <= min) {
        min = dist[v];
        min_index = v;
      }
    }
  }
  return min_index;
}

function sleep(miliseconds) {
  var currentTime = new Date().getTime();

  while (currentTime + miliseconds >= new Date().getTime()) {}
}


/**
 * Function that implements Dijkstra's
 * @param {*} matrix adjacency matrix of graph
 * @param {*} src start node
 * @param {*} nodeCount number of nodes in graph
 * function works by updating the dist and visited arrays that are global
 * every notable action the code executes will also push instructions to steps
 * These instructions are split by a | character and the first part is used 
 * in the visualisation and the second part is an english explaination of 
 * what the algorithm is doing at that particular step
 * Once the algorithm has been executed we will have an array of instructions
 * that are then used in the visualisation as well as creating a table of shortest
 * path weights from start node to every other possible node
 */

function dijkstra(matrix, src, nodeCount) {
  let result = [];
  steps.push(
    "CRE_DIST_VISI_LEN_" +
      nodeCount +
      "|Create a Distance and Visited array of length " +
      nodeCount
  );
  let dist = new Array(nodeCount);

  let visited = new Array(nodeCount);

  // Initialize all distances as
  // INFINITE and stpSet[] as false
  steps.push(
    "SET_DIST_ALL_INF|Set the value of each element in distance array to infinite"
  );
  steps.push(
    "SET_VISI_ALL_false|Set the value of each element in visited array to false"
  );
  //init the dist and visited arrays to starting state
  for (let i = 0; i < nodeCount; i++) {
    dist[i] = Number.MAX_VALUE;
    visited[i] = false;
  }

  // Distance of source vertex
  // from itself is always 0
  dist[src] = 0;
  steps.push("SET_DIST_" + src + "_0|Set the distance of the source node to 0");
  // Find shortest path for all vertices
  for (let count = 0; count < nodeCount; count++) {

    // Pick the minimum distance node from the set of nodes not yet visited
    // u = src on first iteration
    let u = minDistance(dist, visited, nodeCount);
    steps.push(
      "MIN_DIST_" + u + "|Minimum distance of adjacent nodes = Node " + u
    );
    // Mark the picked vertex as processed

    if(u == -1)
    {
      steps.push("FINISH");
      printAlgorithmTable(dist, nodeCount);
      return;
    }else{
      visited[u] = true;
      steps.push(
        "SET_VISI_" + u + "_true|Set node " + u + " to visited in array to true"
      );
    }


    //update the distance array of adjacent nodes
    for (let v = 0; v < nodeCount; v++) {
      //update dist[v] if not visited and theres a connecting edge and its not unvisitable
      if (!visited[v] && matrix[u][v] != 0 && dist[u] != Number.MAX_VALUE &&dist[u] + matrix[u][v] < dist[v]) {
        dist[v] = dist[u] + matrix[u][v];
        steps.push("SET_DIST_" + v + "_" + (dist[u] + matrix[u][v]) + "|Set the distance of of node " + v + " to " + (dist[u] + matrix[u][v]));
      }
    }
  }
  //FINISHED
  steps.push("FINISH");
  printAlgorithmTable(dist, nodeCount);

}

function printAlgorithmTable(dist, nodeCount) {
  var div = document.getElementById("algorithmTable");
  let table =
    `
    <h4>Result:</h4>
    <table class="table table-bordered" style="width: 90%; margin:auto;">
    <thead>
    <th scope="col">Node</th>
    <th scope="col">Distance from node [ ` +
    sessionStorage.getItem("startNode") +
    `
    ]</th>
    `;

  for (let x = 0; x < nodeCount; x++) {
    table += '<tr><th scope="row">' + x + "</th>";
    if (dist[x] == Number.MAX_VALUE) {
      table += "<td>" + "unreachable" + "</td>";
    } else {
      table += "<td>" + dist[x] + "</td>";
    }
    table += "</tr>";
  }
  table += "</tbody>";
  div.innerHTML = table;
}

let kruskalInstructionsArr = [];
let finalUsedEdges = [];
let orderedEdges = [];
let kruskalStep = 0;
let MSTWeight=0;

function kruskalsTable(finalUsedEdges)
{
  let table = document.getElementById("algorithmSteps");

  let innerHTMLString = `<table class="table table-bordered" style="width: 90%; margin:auto;"><thead><th></th>`;
  for(let x = 0; x < orderedEdges.length;x++)
  {
    innerHTMLString+=`<th id="kruskalsTableEdge`+orderedEdges[x][0]+"_"+orderedEdges[x][1]+`">Edge(`+orderedEdges[x][0]+`,`+orderedEdges[x][1]+`)<br> weight=`+orderedEdges[x][2]+"</th>";
  }
  innerHTMLString+=`</thead><tbody><tr><td colspan="`+orderedEdges.length+1+`" id="MSTWeightDiv" style="text-align: center;">Minimum Spanning Tree Weight: 0</td></tr></tbody>`;
  innerHTMLString+=`</table>`;
  table.innerHTML = innerHTMLString;
}

function kruskals()
{
  console.log("Sort edges from least to highest weight");
  sortEdges();
  console.log("Matrix = " , graph.getMatrix());
  //console.log("Kruskal's = " , kruskalsAlgo(graph.getMatrix()));
  let validGraph = true;
  for(let x = 0; x < graph.edges.length;x++)
  {
    if(graph.edges[x].directed===true)
    {
      validGraph=false;
    }
  }
  if(graph.nodes.length===0 || graph.edges.length===0)
  {
    validGraph=false;
  }
  if(validGraph===true)
  {
    document.getElementById("kruskalsButtonDiv").innerHTML+="<h4>To run a visualisation choose play or next</h4>"
    finalUsedEdges, kruskalInstructionsArr = kruskalsAlgo(graph.getMatrix());
    console.log("KruskalInsturctionsArr = " , kruskalInstructionsArr);
    console.log("finalUsedEdges = " , finalUsedEdges);
    kruskalsTable(finalUsedEdges);
    document.getElementById("KruskalsControlButtons").style.display="block";
    document.getElementById("DJsControlButtons").style.display="none";
//    kruskalInstruction_next(true);
  }else{
    console.log("Graph must be undirected");
    alert("Invalid Graph. \nNote:Graph must be undirected for kruskals and graph must be present");
  }


}

function kruskalInstruction_next(play)
{
  let algorithmInstructionDiv = document.getElementById("algorithmStepsText");
  let algorithmTableDiv = document.getElementById("algorithmSteps");

  if(kruskalInstructionsArr[1][kruskalStep].length>1)
  {
    if(kruskalInstructionsArr[1][kruskalStep][0].startsWith("TEST_EDGE"))
    {
      let chosenEdge = kruskalInstructionsArr[1][kruskalStep][0].split("_");
      for(let x = 0; x < graph.edges.length;x++)
      {
        if(graph.edges[x].nodeA.id === parseInt(chosenEdge[2]) && graph.edges[x].nodeB.id === parseInt(chosenEdge[3]))
        {
          graph.edges[x].c = "orange";
          document.getElementById("kruskalsTableEdge"+chosenEdge[2]+"_"+chosenEdge[3]).style.backgroundColor = "orange";
          redrawCanvas();
        }else if(graph.edges[x].nodeA.id === parseInt(chosenEdge[3]) && graph.edges[x].nodeB.id === parseInt(chosenEdge[2]))
        {
          graph.edges[x].c = "orange";
          console.log("kruskalsTableEdge"+chosenEdge[2]+"_"+chosenEdge[3]);
          document.getElementById("kruskalsTableEdge"+chosenEdge[2]+"_"+chosenEdge[3]).style.backgroundColor = "orange";
          redrawCanvas();
        }
      }
      console.log("chosenEdge = " , chosenEdge);
    }else if(kruskalInstructionsArr[1][kruskalStep][0].startsWith("CYCLE_DETECTED"))
    {
      let cycleDetected = kruskalInstructionsArr[1][kruskalStep][0].split("_");
      for(let x = 0; x < graph.edges.length;x++)
      {
        if(graph.edges[x].nodeA.id === parseInt(cycleDetected[2]) && graph.edges[x].nodeB.id === parseInt(cycleDetected[3]))
        {
          graph.edges[x].c = "red";
          document.getElementById("kruskalsTableEdge"+cycleDetected[2]+"_"+cycleDetected[3]).style.backgroundColor = "#FF6961";

          redrawCanvas();
        }else if(graph.edges[x].nodeA.id === parseInt(cycleDetected[3]) && graph.edges[x].nodeB.id === parseInt(cycleDetected[2]))
        {
          graph.edges[x].c = "red";
          document.getElementById("kruskalsTableEdge"+cycleDetected[2]+"_"+cycleDetected[3]).style.backgroundColor = "#FF6961";

          redrawCanvas();
        }
      }
    }else if(kruskalInstructionsArr[1][kruskalStep][0].startsWith("ADD_"))
    {
      let addEdge = kruskalInstructionsArr[1][kruskalStep][0].split("_");
      for(let x = 0; x < graph.edges.length;x++)
      {
        if(graph.edges[x].nodeA.id === parseInt(addEdge[1]) && graph.edges[x].nodeB.id === parseInt(addEdge[2]))
        {
          graph.edges[x].c = "#90EE90";
          console.log("Error1 kruskalsTableEdge"+addEdge[1]+"_"+addEdge[2]);

          document.getElementById("kruskalsTableEdge"+addEdge[1]+"_"+addEdge[2]).style.backgroundColor = "#90EE90";
          MSTWeight+= parseInt(graph.edges[x].weight);
          document.getElementById("MSTWeightDiv").innerHTML="Minimum Spanning Tree weight = " + MSTWeight;
          redrawCanvas();
        }else if(graph.edges[x].nodeA.id === parseInt(addEdge[2]) && graph.edges[x].nodeB.id === parseInt(addEdge[1]))
        {
          graph.edges[x].c = "#90EE90";
          console.log("Error2 kruskalsTableEdge"+addEdge[2]+"_"+addEdge[1]);
          document.getElementById("kruskalsTableEdge"+addEdge[1]+"_"+addEdge[2]).style.backgroundColor = "#90EE90";
          MSTWeight+= parseInt(graph.edges[x].weight);
          document.getElementById("MSTWeightDiv").innerHTML="Minimum Spanning Tree weight = " + MSTWeight;

          redrawCanvas();
        }
      }
    }
    algorithmInstructionDiv.innerHTML = kruskalInstructionsArr[1][kruskalStep][1];

  }else{
    console.log("2");
    algorithmInstructionDiv.innerHTML = kruskalInstructionsArr[1][kruskalStep];
  }
  if(play===true && kruskalStep < kruskalInstructionsArr[1].length)
  {
    if(kruskalInstructionsArr[1][kruskalStep][0] !== "FINISHED")
    {
      kruskalStep++;
      setTimeout(kruskalInstruction_next, 1000, true);
    }
    else{
      console.log("Finished");
    }

  }
  if(play===false && kruskalStep < kruskalInstructionsArr[1].length)
  {
    if(kruskalInstructionsArr[1][kruskalStep][0] !== "FINISHED")
    {
      kruskalStep++;
    }
    else{
      console.log("Finished");
    }

  }
    
}

function kruskalsAlgo(matrix) {
  let instructions = [];
  let edgesArr = []; //init empty edge arr (filled later with )
  let numberOfNodes = matrix.length; //number of nodes in the graph
  let parent = new Array(numberOfNodes).fill(false); //parent array prefilled with -1

  // loop through whole matrix and if matrix[x][y] is not 0 then there is an edge, push this edge to edge array
  for (let x = 0; x < numberOfNodes; x++) 
  {
    for (let y = x + 1; y < numberOfNodes; y++) 
    {
      if (matrix[x][y] !== 0) 
      { //if there is an edge between x and y
        edgesArr.push([x, y, matrix[x][y]]); //push x y and the weight to edgesArr
      }
    }
  }
  edgesArr.sort((a, b) => a[2] - b[2]); //sort edgeArr array by weight of edge
  orderedEdges = edgesArr;
  instructions.push(["SORT_LIST_OF_EDGES","Sort the list of edges" , edgesArr]); //instruction pushed to arr

  let usedEdges = [];
  let count = 0;

  // Find the minimum spanning tree.
  instructions.push(["ITERATE_EDGESARR","Iterate through the sorted list and add edges that do not create a cycle"]);
  for (let [x, y, weightOfEdge] of edgesArr) {
    let rootOfXNode = testCycle(x);
    let rootOfYNode = testCycle(y);
    instructions.push([("TEST_EDGE_"+x+"_"+y+"_"+weightOfEdge),("Testing edge from node "+x+" to node "+y)]);
    if (rootOfXNode !== rootOfYNode) { //if condition is met then a cycle would not be formed so push it to the list of used edges

      parent[rootOfXNode] = rootOfYNode;
      usedEdges.push([x, y]);
      instructions.push([("ADD_"+x+"_"+y),("Add edge from node "+x+" to node "+y+" with weight of "+ weightOfEdge)]);
      count++;
    }else{
      //Adding edge would result in a cycle, therefore do not push
      instructions.push(["CYCLE_DETECTED_"+x+"_"+y,"Adding edge between node "+x+" and "+y+" would cause a cycle. So ignore"])
    }

    if (usedEdges.length === numberOfNodes - 1) { //If the number of added edges is numberOfNodes-1 then we have a connected MST
      instructions.push(["FINISHED","We now have 1 less edge than the number of nodes so we can stop."])
      return [usedEdges,instructions];
    }
  }

  return [usedEdges,instructions];

  // find the root parent node recursively to test if a cycle would be created 
  function testCycle(node) {
    if (parent[node] === false) { //if there is no parent node then return the node
      return node;
    }else{
      return testCycle(parent[node]); //find parent node of this node
    }
    
  }
}


function sortEdges()
{
  //create new array of sorted edges
  let sortedEdges = graph.edges.sort((a,b) => {
    return a.weight.localeCompare(b.weight);
  });
  console.log("edges = " , graph.edges);
  console.log("sorted edges = " , sortedEdges);
}