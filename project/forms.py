from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    EmailField,
    SubmitField,
    validators,
    Form,
    HiddenField,
    BooleanField
)
from wtforms.validators import DataRequired, Email, EqualTo, InputRequired

"""
Login form with username, password and submit. Username and password require input for form to submit
"""
class LoginForm(FlaskForm):
    username = StringField(
        "Username", [validators.DataRequired()], render_kw={"placeholder": "Username"}
    )
    password = PasswordField(
        "Password", [validators.DataRequired()], render_kw={"placeholder": "Password"}
    )
    submit_login = SubmitField("Login")

"""
Register form with username, email, password, confirm pasowrd and submit
All must have values and email must conform to email format
"""
class RegisterForm(FlaskForm):
    username = StringField(
        "Username",
        [validators.DataRequired(message="abc123")],
        render_kw={"placeholder": "Username"},
    )
    email = EmailField(
        "Email", [validators.Email()], render_kw={"placeholder": "Email Address"}
    )
    password = PasswordField(
        "New Password",
        [
            validators.DataRequired(),
            validators.EqualTo("confirm_password", message="Passwords must match!"),
        ],
        render_kw={"placeholder": "Password"},
    )
    confirm_password = PasswordField(
        "Repeat Password", render_kw={"placeholder": "Confirm Password"}
    )
    teacher_check = BooleanField("teacher")
    submit_register = SubmitField("Register")

"""
Save graph form
Takes the name of the graph (input required) and has 2 hidden fields to store node and edge data and a submit button
"""
class SaveGraphForm(FlaskForm):
    graph_name = StringField(
        "Graph Name",
        [validators.DataRequired()],
        render_kw={"placeholder": "Name of Graph"},
    )
    nodes_hidden = HiddenField()
    edges_hidden = HiddenField()
    submit_saveGraph = SubmitField("Save")
