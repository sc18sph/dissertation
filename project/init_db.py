"""
Function for creating users when git repo is cloned to save time creating new users
"""
def fill_db(app, db, Users):
    import random
    import bcrypt

    usernames = ["admin", "sam", "chloe", "finn", "ella", "jude", "kai", "wren", "leo"]
    classes = [1, 2, 1, 1, 1, 1, 2, 2, 2]
    user_types = [3, 2]
    users_to_add = []

    for x in range(len(usernames)):
        hashedPassword = bcrypt.hashpw(
            usernames[x].encode("utf8"), bcrypt.gensalt(rounds=14)
        ).decode("utf8")
        if usernames[x] == "admin":
            user_type = 3
        elif usernames[x] == "sam":
            user_type = 2
        else:
            user_type = 1
        users_to_add.append(
            Users(
                username=usernames[x],
                password=hashedPassword,
                email=str(usernames[x] + "@email.com"),
                classes=classes[x],
                user_type=user_type,
            )
        )

    with app.app_context():
        db.session.add_all(users_to_add)
        db.session.commit()
