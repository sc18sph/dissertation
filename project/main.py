from flask import Blueprint, render_template, redirect, url_for, jsonify, request
from flask_login import login_required, current_user
from . import db
from .models import Graphs, Users, Homeworks, Classes, Results
from datetime import datetime, timedelta
from .forms import SaveGraphForm
import datetime
import json

main = Blueprint("main", __name__)


# Home
@main.route("/")
def index():
    return redirect(url_for("main.home"))

#About page with manuel
@main.route("/about")
def about():
    return redirect("https://drive.google.com/file/d/17orXTfeqVh6w4FmJbIC8aQf5rGKbY7lH/view?usp=sharing")

#Teacher home
@main.route("/home")
@login_required
def home():
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page

    return render_template("home.html", name=current_user.username, title="Home")

#student home
@main.route("/home_student")
@login_required
def home_student():
    if current_user.user_type == 2:
        return redirect(url_for("main.home"))#redirect teacher to teacher page
    return render_template("home_student.html", name=current_user.username, title="Home")

"""
Homework hub for teachers, view homeworks set and redirect to /setHomework
"""
@main.route("/homeworkHUB_teacher")
@login_required
def homeworkHUB_teacher():
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page    
        dateTimeNow = datetime.datetime.utcnow() + timedelta(hours=1)
    getClasses = Users.query.filter_by(id=current_user.id).first()
    try: #If there is a number of classes then splitinto lsit
        classes = getClasses.classes.split("|")
        classes = [int(x) for x in classes]
    except: #If there are no classed
        classes = []
    getHomeworks = Homeworks.query.filter(Homeworks.class_id.in_(classes)).all()
    for homework in getHomeworks:
        homework.class_name = Classes.query.filter_by(id=homework.class_id).first().name

    return render_template(
        "homeworkHUB_teacher.html",
        homeworks=getHomeworks,
        name=current_user.username,
        dateTimeNow=dateTimeNow,
        title="Homework HUB"
    )

"""
View an individual class with grades for homeworks
"""
@main.route("/viewClass/<int:class_id>")
@login_required
def viewClass(class_id):
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page

    getClass = Classes.query.filter_by(id=class_id).first()
    try: #split students into a list
        students = getClass.students.split(",")
    except:# if no students then []
        students = []

    getStudents = Users.query.filter(Users.id.in_(students)).all()
    getHomeworks = Homeworks.query.filter_by(class_id=class_id).all()
    print("getClass = ", getClass)
    print("student = ", students)
    print("getStudents = ", getStudents)
    print("getHomeworks = ", getHomeworks)

    marks = []

    for student in getStudents: #for each student
        student.marks = []
        for homework in getHomeworks: #for each homework
            result = Results.query.filter_by(
                homework_id=homework.id, user_id=student.id
            ).first() #get the result of the students homework 
            if result: #if there is a result append to list
                student.marks.append(result)
            else: #if no result append "no score"
                student.marks.append("No Score")

    print("Marks = ", marks)
    return render_template(
        "viewClass.html",
        Class=getClass,
        students=getStudents,
        homeworks=getHomeworks,
        marks=marks,
        name=current_user.username,
        title="View Class"
        
    )


@main.route("/viewClasses_student")
@login_required
def viewClasses_student():
    if current_user.user_type == 2:
        return redirect(url_for("main.home"))#redirect teacher to teacher page

    getClassIDs = Users.query.filter_by(id=current_user.id).first()
    try: ##split class IDs into a list
        classList = getClassIDs.classes.split(",")
        classList = [int(x) for x in classList]
    except: #if no classes then []
        classList = []
    print(classList)
    getClasses = Classes.query.filter(Classes.id.in_(classList)).all() #get all classes with id in classList
    print(getClasses)
    return render_template(
        "viewClasses_student.html", classes=getClasses, name=current_user.username, title="Classes"
    )

"""
Teacher viewing all classes
"""
@main.route("/viewClasses_teacher", methods=["POST", "GET"])
@login_required
def viewClasses_teacher():
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page
    getClasses = Classes.query.filter_by(teacher_id=current_user.id).all()#get all classes that teachers ID is teacher_id of in classes table
    teachersClasses = []
    for class_ in getClasses: #for each class
        teachersClasses.append(class_.id) #append the class ID to list
        try:
            class_.noStudents = class_.students.count(",") + 1 #count the number of students in the class
        except:
            class_.noStudents = 0

    getHomeworks = Homeworks.query.filter(Homeworks.class_id.in_(teachersClasses)).all() #get all homeworks teacher has set
    homeworkIDs = []
    for homework in getHomeworks: #get all IDs of homeworks teacher has set
        homeworkIDs.append(homework.id)
    try: ##turn classes into a list of lists
        getClasses = [[x] for x in getClasses]
    except:
        getClasses = []
    results_table = []
    for class_ in getClasses:
        for homework in getHomeworks:
            if class_[0].id == homework.class_id:
                class_.append(homework) #for each homework append it to the class that it is assigned to

    getResults = Results.query.filter(Results.homework_id.in_(homeworkIDs)).all() #get all results from all homeworks

    for result in getResults:
        for homeworks in getHomeworks:
            print("homework >>>", homeworks.class_id)
            if result.homework_id == homeworks.id:
                result.homeworkName = homeworks.name #get the name of the homeowkr
                result.class_id = homeworks.class_id #get the id of the class the homework was assigned to
                print(homeworks.id, " ", homeworks.class_id)
    print("getResults = ", getResults)

    print("getClasses = ", getClasses)
    print("teachersClasses = ", teachersClasses)
    print("getHomeworks = ", getHomeworks)
    print("homeworkIDs = ", homeworkIDs)

    return render_template(
        "viewClasses_teacher.html",
        classes=getClasses,
        results=getResults,
        name=current_user.username,
        dateTimeNow=datetime.datetime.utcnow() + timedelta(hours=1), title="Classes"
    )

"""
POST route used when student submits a class code to join
"""
@main.route("/joinClassPOST", methods=["POST"])
@login_required
def joinClassPOST():
    data = request.get_json() #get the data using AJAX from the Javascript XML request
    classCode = data["classCode"] #class code input by student

    getClasses = Classes.query.filter_by(class_code=classCode).first() #check if there is a class with that code
    if getClasses: #if there is a class with the supplied code
        print("adding user to class")
        print(getClasses)
        changeUserClass = Users.query.filter_by(id=current_user.id).first()
        if getClasses.id in [int(x) for x in changeUserClass.classes.split(",")]: #if the users ID is already in the list of students enrolled in the class
            print("User already in class")
            return jsonify({"result": "Already enrolled in class"}) #return result
        print(changeUserClass)
        if len(changeUserClass.classes) > 0: #if student has enrolled in previous classes. add , before class ID
            changeUserClass.classes = changeUserClass.classes + "," + str(getClasses.id)
        else:
            changeUserClass.classes = changeUserClass.classes + str(getClasses.id) #if tstudent is not enrolled in any classse
        if len(getClasses.students) > 0: #if class is not empty then need to add , before students id
            getClasses.students = getClasses.students + "," + str(current_user.id)
        else: #if class is empty only need to add student id
            getClasses.students = getClasses.students + str(current_user.id)
        db.session.commit() #commit changes to DB
        return jsonify({"result": "success"}) #return result

    else: #if a class doesnt exist w/ that class_code
        print("No class exists with class_code of ", classCode)
        return jsonify({"result": "Class does not exist"}) #return result

"""
Route used for teacher creating a new class
"""
@main.route("/viewClasses_teacherPOST", methods=["POST"])
@login_required
def viewClasses_teacherPOST():
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page
    data = request.get_json() #get the Javascript XML request
    newClassName = data["newClassName"] #get the new Class name the teacher wants to create
    if Classes.query.filter_by(name=newClassName).all(): #if a class already exists with that name
        print("Class already exits")
        return jsonify({"result": "Class already exists"})#return result

    else: #if a class doesnt exist
        import random
        import string
        try: #get a list of all class codes currently being used
            getClassCodes = Classes.query.all()
            getClassCodesList = [class_.class_code for class_ in getClassCodes]
        except: #if there are no classes currently created then list is empty
            getClassCodesList = []
            getClassCodes = []
        # Create a random class code of length 5, used for joining the class
        newClassCode = "".join(
            random.choice(string.ascii_letters + string.digits) for x in range(5)
        )

        # If randomly create a code that already exists keep creating until it is unique
        while newClassCode in getClassCodesList:
            newClassCode = "".join(
                random.choice(string.ascii_letters + string.digits) for x in range(5)
            )

        print(getClassCodesList)

        #new class query
        newClass = Classes(
            teacher_id=current_user.id, name=newClassName, class_code=newClassCode
        )


        db.session.add(newClass)
        """
        Need to find the ID of the new class before commiting changes
        So we can add the class ID to the teachers record
        """
        if(len(getClassCodes)==0): #if there are currently no classes in database then classID =1
            getClassID = 1
        else:
            getClassID = getClassCodes[len(getClassCodes)-1].id+1 #if there are classes in DB then new class ID will be number of classes+1
        updateTeachersClasses = Users.query.filter_by(id=current_user.id).first()
        if(updateTeachersClasses.classes == None): #aadd the new class id to the teachers taught classes
            updateTeachersClasses.classes=getClassID
        else: 
            updateTeachersClasses.classes=updateTeachersClasses.classes+","+str(getClassID)

        db.session.flush()
        db.session.commit()


        print("Class created!")

        return jsonify({"result": "success"}) #return result


"""
Homework HUB for students, lists all/current/previous homework assignments 
provide a link to attempt current homeworks
"""
@main.route("/homeworkHUB_student")
@login_required
def homeworkHUB_student():
    if current_user.user_type == 2:
        return redirect(url_for("main.home"))#redirect teacher to teacher page

    dateTimeNow = datetime.datetime.utcnow() + timedelta(hours=1) #convert from utc to british time
    print("DateTimeNow = " , dateTimeNow)
    getClasses = Users.query.filter_by(id=current_user.id).first() ##get the record of the current user
    print(getClasses.classes)
    try: #if the student is enrolled in classes
        classes = getClasses.classes.split(",") 
    except:#if student is not enrolled in any classes
        classes = []
    print(classes)
    classes = [int(x) for x in classes]#turn ID of claasses enrolled in into list
    print(classes)
    getHomeworks = Homeworks.query.filter(Homeworks.class_id.in_(classes)).all() #get all homeworks for all enrolledin classes
    getResults = Results.query.filter_by(user_id=current_user.id).all() #get all users results
    currentHomeworks = [] #store current homework assignments
    pastHomeworks = [] #store past homework assignemtns
    for homeworkItem in getHomeworks: #for each homework
        for result in getResults: #for each result
            if result.homework_id == homeworkItem.id:#if the result is for the homework
                homeworkItem.result = result.marks #add the mark from result to the homework
        if homeworkItem.dueDate < dateTimeNow: #if homework due date is in the past append to pastHomeworks list
            pastHomeworks.append(homeworkItem)
        else: #if homework due date is not in past append to current homeworks
            currentHomeworks.append(homeworkItem)
    print("pastHomeworks = ", pastHomeworks)
    print("currentHomeworks = ", currentHomeworks)
    print(getHomeworks)
    return render_template(
        "homeworkHUB_student.html",
        homeworks=getHomeworks,
        name=current_user.username,
        dateTimeNow=dateTimeNow,
        pastHomeworks=pastHomeworks,
        currentHomeworks=currentHomeworks,
        title="Homework HUB"
    )

"""
Route for students to attempt homework assignments
"""
@main.route("/attemptHomework/<int:homework_id>")
@login_required
def attemptHomework(homework_id):
    if current_user.user_type == 2:
        return redirect(url_for("main.home"))#redirect teacher to teacher page

    getHomework = Homeworks.query.filter_by(id=homework_id).first() #get the homework record in database
    questions = getHomework.questions.split("|") #split the questions into a list
    algorithms = getHomework.algorithms.split("|") #split the algorithms to be used into a likst
    startNodes = getHomework.startNodes.split("|") #split the start nodes intoa  list
    endNodes = getHomework.endNodes.split("|") #split the end nodes into a list
    marks = getHomework.marks.split("|") #split the marks for each question into a list

    graphsIDs = getHomework.graphs.split("|") #split the graph IDs of graphs used in homework into a list

    print("graphsIDs===", graphsIDs)
    print("getHomework.graphs = ", getHomework.graphs)
    graphsIDs = [int(x) for x in graphsIDs] #turn graphIDs into a list of ints

    """
    Class defined to store homework questions as individual objects
    """
    class HomeworkQuestions:
        def __init__(
            self,
            questionNumber,
            questions,
            algorithms,
            startNodes,
            endNodes,
            marks,
            nodes,
            edges,
        ):
            self.questionNumber = questionNumber
            self.questions = questions
            self.algorithms = algorithms
            self.startNodes = startNodes
            self.endNodes = endNodes
            self.marks = marks
            self.nodes = nodes
            self.edges = edges

    homework_objects = []
    getGraphs = []
    print("graphsIDs = ", graphsIDs)
    for graphID in graphsIDs:
        graph = Graphs.query.filter_by(id=graphID).first()
        if graph:
            getGraphs.append(graph) #get list of graphs
    print("Graphs", getGraphs)
    startNodes_list = []
    endNodes_list = []
    """
    Create a new instance of homeworkQuestions for each question set
    """
    for i in range(len(questions)):
        print(i)
        #homework_object = the data needed for each question
        homework_object = HomeworkQuestions(
            i + 1,
            questions[i],
            algorithms[i],
            startNodes[i],  #####
            endNodes[i],
            marks[i],
            getGraphs[i].nodes,
            getGraphs[i].edges,
        )
        homework_objects.append(homework_object) ##append object to list
        startNodes_list.append(startNodes[i]) #append startNodes to list
        endNodes_list.append(endNodes[i])#append end nodes to list

    """Used for Debugging"""
    getGraphs = Graphs.query.filter(Graphs.id.in_(graphsIDs)).all()
    print(graphsIDs)
    print(getGraphs)
    for graph in getGraphs:
        print(graph.name)
        print(graph.nodes)
        print(graph.edges)

    return render_template(
        "attemptHomework.html",
        homework_id=homework_id,
        homework=getHomework,
        name=current_user.username,
        questions=questions,
        homework_objects=homework_objects,
        algorithms=algorithms,
        startNodes_list=startNodes_list,
        endNodes_list=endNodes_list,
        title="Homework"
    )

"""
POST route called when user submits their homework
Used for marking and adding resultto database
"""
@main.route("/submitHomework", methods=["POST"])
def submitHomework():
    # getMarks

    # getHomeworkID to get custom Question Ansnwers
    data = request.get_json() #get the Javascript XML request
    userAnswers = data["userAnswers"] #get Users answer
    correctAnswers = data["correctAnswers"] #get the correct answer calculated from javascript functions of algorithms
    homework_id = data["homework_id"] #get the ID of the homework
    print(userAnswers)

    getHomework = Homeworks.query.filter_by(id=homework_id).first() #get the homework being attempted
    getHomeworkAnswers = getHomework.answers #get answers from homework
    getHomeworkAnswersList = getHomeworkAnswers.split("|") 

    getHomeworkMarks = getHomework.marks #get marks for each question
    getHomeworkMarksList = getHomeworkMarks.split("|")

    print("GetHomework.answers = ", getHomeworkAnswers)
    print("GetHomeworkMarksList = ", getHomeworkMarksList)
    getHomeworkMarksList = [int(x) for x in getHomeworkMarksList] #list of marks for each question
    print("GetHomeworkMarksList = ", getHomeworkMarksList)
    totalMarksForHomework = sum(getHomeworkMarksList) #total marks for homework

    for x in range(len(correctAnswers)): #for each answer
        if correctAnswers[x] == "___CUSTOM_ANSWER___": #if teacher has chosen a custom question&answer
            correctAnswers[x] = getHomeworkAnswersList[x] #set the correct answer for that Q to the A given by teacher
    for questionAnswer in userAnswers: #for of answer
        for answerNum in questionAnswer: 
            if answerNum == "n/a": #2 answer boxes are created, if only 1 is needed then remove the unneeded answer
                questionAnswer.remove(answerNum)
    print("User Answers = ", userAnswers)
    for x in range(len(correctAnswers)):
        for y in range(len(correctAnswers[x])):
            correctAnswers[x][y] = str(correctAnswers[x][y]) #create a 2D array of correct answers [0][0] is part 1 of q 1 [0][1] is part 2 of q 1
    print("Correct Answers = ", correctAnswers)
    print("HomeworkID  = ", homework_id)

    partMarks = []
    questionMarks = []
    for x in range(len(userAnswers)):
        tmp_questionMarks = []
        for y in range(len(userAnswers[x])):
            if userAnswers[x][y] == correctAnswers[x][y]: #if students answer equals corerct answer
                # questionMarks.append("correct")
                tmp_questionMarks.append(getHomeworkMarksList[x] / len(userAnswers[x])) #add the marks for that question to tmp_questionMarks
            else:
                # questionMarks.append("incorrect")
                tmp_questionMarks.append(0) #if wrong add 0 fo marks
        partMarks.append(tmp_questionMarks)
        questionMarks.append(sum(tmp_questionMarks))

    print("Results = ", partMarks, "\n > ", questionMarks)
    print("Result = ", sum(questionMarks), "/", totalMarksForHomework)
    #new insert query to add the results for homework to database
    submitResult = Results(
        user_id=current_user.id,
        homework_id=homework_id,
        marks=sum(questionMarks),
        maxMarks=totalMarksForHomework,
    )
    db.session.add(submitResult)
    db.session.commit()

    return jsonify({"result": "success"})


@main.route("/setHomeworkPOST", methods=["POST"])
def setHomeworkPOST():
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page

    data = request.get_json()
    """
    Gather all information from the homework submission form and split into lists where applicable
    """
    homeworkName = data["homeworkName"]
    chosenClass = data["chosenClass"]
    startNodes = "|".join(data["startNodes"])
    endNodes = "|".join(data["endNodes"])
    selectedGraphs = data["selectedGraphs"]
    algorithms = "|".join(data["algorithms"])
    questions = "|".join(data["questionTypes"])
    customAnswers = "|".join(data["customAnswers"])
    marks = "|".join(data["marks"])

    setDateTime = data["setDateTime"]
    dueDateTime = data["dueDateTime"]

    # get class record of the chosen class
    getClassID = Classes.query.filter_by(name=chosenClass).first()
    from datetime import datetime

    setDateList = setDateTime.split("|")
    dueDateList = dueDateTime.split("|")
    """
    Convert the datetime objects for set and due dates into datetime objects
    """
    setDateYear, setDateMonth, setDateDay = map(int, setDateList[0].split("-"))
    dueDateYear, dueDateMonth, dueDateDay = map(int, dueDateList[0].split("-"))
    setTimeHr, setTimeMin = map(int, setDateList[1].split(":"))
    dueTimeHr, dueTimeMin = map(int, dueDateList[1].split(":"))
    setDateTimeObj = datetime(
        setDateYear, setDateMonth, setDateDay, setTimeHr, setTimeMin
    )
    dueDateTimeObj = datetime(
        dueDateYear, dueDateMonth, dueDateDay, dueTimeHr, dueTimeMin
    )

    print("Class ID = ", getClassID.id)
    print(setDateList)
    print(dueDateList)

    """
    Get the graph IDs for all the selected graphs and append to a list called graphIDs
    """
    graphIDs = Graphs.query.filter(Graphs.name.in_(selectedGraphs)).all()

    graphIDs = []
    for name in selectedGraphs:
        graph = Graphs.query.filter_by(name=name).first()
        if graph:
            graphIDs.append(graph.id)

    print("graphIDs= ", graphIDs)

    selectedGraphs = "|".join(str(x) for x in graphIDs)
    print("selectedGraphs = ", selectedGraphs)

    #New homework query with all information about assignment
    newHomework = Homeworks(
        class_id=getClassID.id,
        name=homeworkName,
        questions=questions,
        algorithms=algorithms,
        graphs=selectedGraphs,
        setDate=setDateTimeObj,
        dueDate=dueDateTimeObj,
        answers=customAnswers,
        startNodes=startNodes,
        endNodes=endNodes,
        marks=marks,
    )
    print(newHomework)
    """
    Add new homework to database
    """
    db.session.add(newHomework)
    db.session.commit()

    return jsonify({"result": "success"}) #return success

"""
Graph route
When saving a graph the nodes and edges JSOn objects are stored in a hidden form in hidden fields
When the form is submitted a new graph query is created and inserted into the database
"""
@main.route("/graphs", methods=["GET", "POST"])
@login_required
def graphs():
    save_form = SaveGraphForm()
    if save_form.submit_saveGraph.data:
        #new graph query
        saveGraph = Graphs(
            user_id=current_user.id,
            name=save_form.graph_name.data,
            created_date=datetime.datetime.now(),
            edges=save_form.edges_hidden.data,
            nodes=save_form.nodes_hidden.data,
        )
        #submit to db
        db.session.add(saveGraph)
        db.session.commit()
    #get all thegraphs the user has saved to display in load dropdown
    getGraphs = Graphs.query.filter_by(user_id=current_user.id).all()
    print("Graphs = ", getGraphs)
    print("UserID = ", current_user.id)
    return render_template(
        "graphs.html",
        SavedGraphs=getGraphs,
        save_form=save_form,
        name=current_user.username,
        title="Graph Tool"
    )

"""
Route for the setHomework form
"""
@main.route("/setHomework", methods=["GET", "POST"])
@login_required
def setHomework():
    if current_user.user_type == 1:
        return redirect(url_for("main.home_student"))#redirect student to student page
    #get all the graphs and classes associated with teh teacher
    getGraphs = Graphs.query.filter_by(user_id=current_user.id).all()
    getClasses = Users.query.filter_by(id=current_user.id).first()
    try: #if the teacher is incharge of classes
        classes = getClasses.classes
        classes = classes.split(",")
        classes = [int(x) for x in classes] #split class IDs into list
        getClassNames = Classes.query.filter_by(teacher_id=current_user.id).all()#get the names of all the classes with id in list
        classNames = [x.name for x in getClassNames]#split names into list
    except Exception as e:#if teacher has no classes set to empty
        #they wont be able to submit the form without a class selected
        print("Exception = " , e)
        classes = []
        classNames = []

    graphNames = []
    graphNumNodes = []
    #get the infromation about the graphs the teacher has saved
    for x in getGraphs:
        graphNames.append(x.name) #name of graphs in list
        length = len(json.loads(x.nodes))
        graphNumNodes.append(length) #get the number of nodes for each graph
        # graphNumNodes.append(x.)
    return render_template(
        "setHomework.html",
        name=current_user.username,
        graphs=getGraphs,
        graphNames=graphNames,
        graphNumNodes=graphNumNodes,
        classes=classNames,
        title="Set Homework"
    )
