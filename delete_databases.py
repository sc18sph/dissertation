"""
Python script to delete all databases
Primarily used in testing
"""

from project import *
app=create_app()
app.app_context().push()
db.drop_all()